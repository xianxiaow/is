const isType = (type) => {
  return (obj): boolean => {
    return {}.toString.call(obj) === `[object ${type}]`;
  }
}
const owns = Object.prototype.hasOwnProperty;

/**
 * @param v any
 * @return boolean
 */
export const isObject = isType('Object');
/**
 * @param v any
 * @return boolean
 */
export const isString = isType('String');
/**
 * @param v any
 * @return boolean
 */
export const isArray = Array.isArray;
/**
 * @param v any
 * @return boolean
 */
export const isFunction = isType('Function');
/**
 * @param v any
 * @return boolean
 */
export const isDate = isType('Date');
/**
 * @param v any
 * @return boolean
 */
export const isNumber = isType('Number');
/**
 * @param v any
 * @return boolean
 */
export const isUndef = isType('Undefined');
/**
 * @param v any
 * @return boolean
 */
export const isBoolean = isType('Boolean');
/**
 * @param v any
 * @return boolean
 */
export const isRegExp = isType('RegExp');
/**
 * 判断是否是空对象
 * @param v any
 * @return boolean
 */
export const isEmptyObject = (value): boolean => {
  if (isObject(value)) {
    return Object.keys(value).length === 0;
  }
  return false;
}
