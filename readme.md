# tiny-is

This Brower / Node.js library that check type of variable

# install

```bash
npm i tiny-is
```

# API

```javascript
const {
    isObject,
    isString
    isArray
    isFunction
    isDate
    isNumber
    isUndef
    isBoolean
    isRegExp
    isEmptyObject
} = require('tiny-is');
```
