var isType = function (type) {
    return function (obj) {
        return {}.toString.call(obj) === "[object " + type + "]";
    };
};
var owns = Object.prototype.hasOwnProperty;
/**
 * @param v any
 * @return boolean
 */
export var isObject = isType('Object');
/**
 * @param v any
 * @return boolean
 */
export var isString = isType('String');
/**
 * @param v any
 * @return boolean
 */
export var isArray = Array.isArray;
/**
 * @param v any
 * @return boolean
 */
export var isFunction = isType('Function');
/**
 * @param v any
 * @return boolean
 */
export var isDate = isType('Date');
/**
 * @param v any
 * @return boolean
 */
export var isNumber = isType('Number');
/**
 * @param v any
 * @return boolean
 */
export var isUndef = isType('Undefined');
/**
 * @param v any
 * @return boolean
 */
export var isBoolean = isType('Boolean');
/**
 * @param v any
 * @return boolean
 */
export var isRegExp = isType('RegExp');
/**
 * 判断是否是空对象
 * @param v any
 * @return boolean
 */
export var isEmptyObject = function (value) {
    if (isObject(value)) {
        return Object.keys(value).length === 0;
    }
    return false;
};
