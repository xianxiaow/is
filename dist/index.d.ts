/**
 * @param v any
 * @return boolean
 */
export declare const isObject: (obj: any) => boolean;
/**
 * @param v any
 * @return boolean
 */
export declare const isString: (obj: any) => boolean;
/**
 * @param v any
 * @return boolean
 */
export declare const isArray: (arg: any) => arg is any[];
/**
 * @param v any
 * @return boolean
 */
export declare const isFunction: (obj: any) => boolean;
/**
 * @param v any
 * @return boolean
 */
export declare const isDate: (obj: any) => boolean;
/**
 * @param v any
 * @return boolean
 */
export declare const isNumber: (obj: any) => boolean;
/**
 * @param v any
 * @return boolean
 */
export declare const isUndef: (obj: any) => boolean;
/**
 * @param v any
 * @return boolean
 */
export declare const isBoolean: (obj: any) => boolean;
/**
 * @param v any
 * @return boolean
 */
export declare const isRegExp: (obj: any) => boolean;
/**
 * 判断是否是空对象
 * @param v any
 * @return boolean
 */
export declare const isEmptyObject: (value: any) => boolean;
